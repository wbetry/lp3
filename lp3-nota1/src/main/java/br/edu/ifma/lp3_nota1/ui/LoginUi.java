package br.edu.ifma.lp3_nota1.ui;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginUi {

    private JInternalFrame janelaLogin;
    private JPanel painelPrincipal;

    public void montaTela(JDesktopPane painelMdi) {
        prepararJanela(painelMdi);
        prepararPainelprincipal();
        prepararLogin(painelMdi);
        mostrarJanela();
    }

    private void prepararJanela(JDesktopPane painelMdi) {
        janelaLogin = new JInternalFrame("Login");
        painelMdi.add(janelaLogin);
    }

    private void prepararPainelprincipal() {
        painelPrincipal = new JPanel();
        janelaLogin.add(painelPrincipal);
    }

    private void prepararLogin(JDesktopPane painelMdi) {
        painelPrincipal.setLayout(new GridLayout(5, 1   ));
        JLabel labelLogin = new JLabel("Login");
        JTextField inputLogin = new JTextField(20);
        JPanel painel1 = new JPanel();
        painel1.add(labelLogin);
        painel1.add(inputLogin);
        JLabel labelSenha = new JLabel("Senha");
        JPasswordField inputSenha = new JPasswordField(20);
        JPanel painel2 = new JPanel();
        painel2.add(labelSenha);
        painel2.add(inputSenha);
        JButton botaoEntrar = new JButton("Entrar");
        botaoEntrar.addActionListener((e) -> {
            painelMdi.remove(janelaLogin);
            painelMdi.repaint();
        });
        JButton botaoSair = new JButton("Cancelar");
        botaoSair.addActionListener((e) -> System.exit(0));
        JPanel painel3 = new JPanel();
        painel3.add(botaoEntrar);
        painel3.add(botaoSair);
        painelPrincipal.add(new JPanel());
        painelPrincipal.add(painel1);
        painelPrincipal.add(painel2);
        painelPrincipal.add(painel3);
        painelPrincipal.add(new JPanel());
    }

    private void mostrarJanela() {
        janelaLogin.pack();
        janelaLogin.setSize(500, 300);
        janelaLogin.setVisible(true);
    }

}