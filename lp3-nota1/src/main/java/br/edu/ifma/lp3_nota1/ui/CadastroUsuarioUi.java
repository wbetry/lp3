package br.edu.ifma.lp3_nota1.ui;

import java.awt.BorderLayout;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class CadastroUsuarioUi {

    private JInternalFrame janelaCadastroUsuario;
    private JPanel painelPrincipal;

    public void montaTela(JDesktopPane painelMdi) {
        prepararJanela(painelMdi);
        prepararPainelprincipal();
        prepararCadastroUsuario(painelMdi);
        mostrarJanela();
    }

    private void prepararJanela(JDesktopPane painelMdi) {
        janelaCadastroUsuario = new JInternalFrame("Cadastro de Usuário", true, true, false);
        painelMdi.add(janelaCadastroUsuario);
    }

    private void prepararPainelprincipal() {
        painelPrincipal = new JPanel();
        janelaCadastroUsuario.add(painelPrincipal);
    }

    private void prepararCadastroUsuario(JDesktopPane painelMdi) {

        JLabel labelNome = new JLabel("Nome");
        JTextField inputNome = new JTextField(20);
        JLabel labelCpf = new JLabel("CPF");
        JTextField inputCpf = new JTextField(20);
        JLabel labelRg = new JLabel("RG");
        JTextField inputRg = new JTextField(20);
        JLabel labelSexo = new JLabel("Sexo");
        ButtonGroup grupoRadio = new ButtonGroup();
        JRadioButton radioMasculino = new JRadioButton("M", false);
        JRadioButton radioFeminino = new JRadioButton("F", false);
        grupoRadio.add(radioMasculino);
        grupoRadio.add(radioFeminino);
        JLabel labelTelefone = new JLabel("Telefone");
        JTextField inputTelefone = new JTextField();
        JLabel labelEmail = new JLabel("Email");
        JTextField inputEmail = new JTextField();
        JLabel labelEndereco = new JLabel("Endereço");
        JTextField inputEndereco = new JTextField();
        JLabel labelComplemento = new JLabel("Complemento");
        JTextField inputComplemento = new JTextField();
        JLabel labelCidade = new JLabel("Cidade");
        JTextField inputCidade = new JTextField();
        JLabel labelEstado = new JLabel("Estado");
        List<String> estadosList = Arrays.asList("AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS",
                "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO");
        String[] estados = estadosList.toArray(new String[estadosList.size()]);
        JComboBox<String> inputEstado = new JComboBox<>(estados);
        JLabel labelObservacoes = new JLabel("Observações");
        JTextArea inputObservacoes = new JTextArea();
        JButton botaoSalvar = new JButton("Salvar");
        JButton botaoCancelar = new JButton("Cancelar");

        int alinhadoEsquerda = 20;
        int somaAltura = 20;

        labelNome.setBounds(alinhadoEsquerda, alinhadoEsquerda, 300, 20);
        inputNome.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 300, 20);
        labelSexo.setBounds(alinhadoEsquerda + 340, alinhadoEsquerda, 600, 20);
        radioMasculino.setBounds(alinhadoEsquerda + 340, alinhadoEsquerda + somaAltura, 60, 20);
        radioFeminino.setBounds(alinhadoEsquerda + 400, alinhadoEsquerda + somaAltura, 60, 20);
        somaAltura += 30;
        labelEndereco.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 200, 20);
        somaAltura += 20;
        inputEndereco.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 440, 20);
        somaAltura += 30;
        labelComplemento.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 440, 20);
        somaAltura += 20;
        inputComplemento.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 440, 20);
        somaAltura += 30;
        labelCidade.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 200, 20);
        labelEstado.setBounds(alinhadoEsquerda + 240, alinhadoEsquerda + somaAltura, 200, 20);
        somaAltura += 20;
        inputCidade.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 200, 20);
        inputEstado.setBounds(alinhadoEsquerda + 240, alinhadoEsquerda + somaAltura, 200, 20);
        somaAltura += 30;
        labelRg.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 200, 20);
        labelCpf.setBounds(alinhadoEsquerda + 240, alinhadoEsquerda + somaAltura, 200, 20);
        somaAltura += 20;
        inputRg.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 200, 20);
        inputCpf.setBounds(alinhadoEsquerda + 240, alinhadoEsquerda + somaAltura, 200, 20);
        somaAltura += 30;
        labelEmail.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 200, 20);
        labelTelefone.setBounds(alinhadoEsquerda + 240, alinhadoEsquerda + somaAltura, 200, 20);
        somaAltura += 20;
        inputEmail.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 200, 20);
        inputTelefone.setBounds(alinhadoEsquerda + 240, alinhadoEsquerda + somaAltura, 200, 20);
        somaAltura += 30;
        labelObservacoes.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 440, 20);
        somaAltura += 20;
        inputObservacoes.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 440, 160);
        somaAltura += 30;

        painelPrincipal.setLayout(null);

        painelPrincipal.add(labelNome);
        painelPrincipal.add(inputNome);
        painelPrincipal.add(labelSexo);
        painelPrincipal.add(radioMasculino);
        painelPrincipal.add(radioFeminino);
        painelPrincipal.add(labelCpf);
        painelPrincipal.add(inputCpf);
        painelPrincipal.add(labelRg);
        painelPrincipal.add(inputRg);
        painelPrincipal.add(labelTelefone);
        painelPrincipal.add(inputTelefone);
        painelPrincipal.add(labelEmail);
        painelPrincipal.add(inputEmail);
        painelPrincipal.add(labelEndereco);
        painelPrincipal.add(inputEndereco);
        painelPrincipal.add(labelComplemento);
        painelPrincipal.add(inputComplemento);
        painelPrincipal.add(labelCidade);
        painelPrincipal.add(inputCidade);
        painelPrincipal.add(labelEstado);
        painelPrincipal.add(inputEstado);
        painelPrincipal.add(labelObservacoes);
        painelPrincipal.add(inputObservacoes);

        JPanel painelBotoes = new JPanel();
        painelBotoes.add(botaoSalvar);
        botaoSalvar.addActionListener((e) -> {
            try {
                janelaCadastroUsuario.setClosed(true);
            } catch (PropertyVetoException e1) {
                e1.printStackTrace();
            }
        });
        painelBotoes.add(botaoCancelar);
        botaoCancelar.addActionListener((e) -> {
            try {
                janelaCadastroUsuario.setClosed(true);
            } catch (PropertyVetoException e1) {
                e1.printStackTrace();
            }
        });
        janelaCadastroUsuario.add(painelBotoes, BorderLayout.SOUTH);
    }

    private void mostrarJanela() {
        janelaCadastroUsuario.pack();
        janelaCadastroUsuario.setSize(800, 600);
        janelaCadastroUsuario.setVisible(true);
    }

}
