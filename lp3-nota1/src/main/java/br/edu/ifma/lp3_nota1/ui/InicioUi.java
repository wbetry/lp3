package br.edu.ifma.lp3_nota1.ui;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class InicioUi {

    private JFrame janela;
    private JDesktopPane painelPrincipal;

    public void montaTela() {
        prepararJanela();
        prepararBarraDeMenu();
        prepararPainelprincipal();
        mostrarJanela();
    }

    private void prepararJanela() {
        janela = new JFrame("Nota 1 LP3");
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void prepararBarraDeMenu() {
        JMenuBar barraDeMenu = new JMenuBar();
        janela.setJMenuBar(barraDeMenu);

        JMenu menuDeArquivo = new JMenu("Arquivo");
        barraDeMenu.add(menuDeArquivo);
        JMenu menuDeUsuario = new JMenu("Usuário");
        barraDeMenu.add(menuDeUsuario);
        JMenu menuDeProduto = new JMenu("Produto");
        barraDeMenu.add(menuDeProduto);
        JMenu menuDeSobre = new JMenu("Sobre");
        barraDeMenu.add(menuDeSobre);

        JMenuItem novoAction = new JMenuItem("Novo");
        menuDeArquivo.add(novoAction);
        JMenuItem abrirAction = new JMenuItem("Abrir");
        menuDeArquivo.add(abrirAction);
        JMenuItem sairAction = new JMenuItem("Sair");
        sairAction.addActionListener((e) -> System.exit(0));
        menuDeArquivo.add(sairAction);

        JMenuItem novoUsuario = new JMenuItem("Cadastrar Usuário");
        novoUsuario.addActionListener((e) -> new CadastroUsuarioUi().montaTela(painelPrincipal));
        menuDeUsuario.add(novoUsuario);

        JMenuItem novoProduto = new JMenuItem("Cadastrar Produto");
        novoProduto.addActionListener((e) -> new CadastroProdutoUi().montaTela(painelPrincipal));
        menuDeProduto.add(novoProduto);

        JMenuItem sobreAction = new JMenuItem("Sobre");
        menuDeSobre.add(sobreAction);
    }

    private void prepararPainelprincipal() {
        painelPrincipal = new JDesktopPane();
        janela.add(painelPrincipal);
        new LoginUi().montaTela(painelPrincipal);
    }

    private void mostrarJanela() {
        janela.pack();
        janela.setSize(1200, 800);
        janela.setResizable(false);
        janela.setVisible(true);
    }

}