package br.edu.ifma.lp3_nota1.ui;

import java.awt.BorderLayout;
import java.beans.PropertyVetoException;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class CadastroProdutoUi {

    private JInternalFrame janelaCadastroProduto;
    private JPanel painelPrincipal;

    public void montaTela(JDesktopPane painelMdi) {
        prepararJanela(painelMdi);
        prepararPainelprincipal();
        prepararCadastroProduto(painelMdi);
        mostrarJanela();
    }

    private void prepararJanela(JDesktopPane painelMdi) {
        janelaCadastroProduto = new JInternalFrame("Cadastro de Produto", true, true, false);
        painelMdi.add(janelaCadastroProduto);
    }

    private void prepararPainelprincipal() {
        painelPrincipal = new JPanel();
        janelaCadastroProduto.add(painelPrincipal);
    }

    private void prepararCadastroProduto(JDesktopPane painelMdi) {
        JLabel labelNome = new JLabel("Nome");
        JTextField inputNome = new JTextField(20);
        JLabel labelEstoque = new JLabel("Estoque");
        JTextField inputEstoque = new JTextField(20);
        JLabel labelDataDeCadastro = new JLabel("Data de Cadastro");
        JTextField inputDataDeCadastro = new JTextField(20);
        JLabel labelFornecedor = new JLabel("Fornecedor");
        JTextField inputFornecedor = new JTextField();
        JLabel labelDescricao = new JLabel("Descrição");
        JTextArea inputDescricao = new JTextArea();
        JButton botaoSalvar = new JButton("Salvar");
        JButton botaoCancelar = new JButton("Cancelar");

        int alinhadoEsquerda = 20;
        int somaAltura = 20;

        labelNome.setBounds(alinhadoEsquerda, alinhadoEsquerda, 440, 20);
        inputNome.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 440, 20);
        somaAltura += 30;
        labelFornecedor.setBounds(alinhadoEsquerda, alinhadoEsquerda, 600, 20);
        somaAltura += 20;
        inputFornecedor.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 440, 20);
        somaAltura += 30;
        labelDataDeCadastro.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 200, 20);
        labelEstoque.setBounds(alinhadoEsquerda + 240, alinhadoEsquerda + somaAltura, 200, 20);
        somaAltura += 20;
        inputDataDeCadastro.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 200, 20);
        inputEstoque.setBounds(alinhadoEsquerda + 240, alinhadoEsquerda + somaAltura, 200, 20);
        somaAltura += 30;
        labelDescricao.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 440, 20);
        somaAltura += 20;
        inputDescricao.setBounds(alinhadoEsquerda, alinhadoEsquerda + somaAltura, 440, 160);
        somaAltura += 30;

        painelPrincipal.setLayout(null);

        painelPrincipal.add(labelNome);
        painelPrincipal.add(inputNome);
        painelPrincipal.add(labelFornecedor);
        painelPrincipal.add(labelEstoque);
        painelPrincipal.add(inputEstoque);
        painelPrincipal.add(labelDataDeCadastro);
        painelPrincipal.add(inputDataDeCadastro);
        painelPrincipal.add(inputFornecedor);
        painelPrincipal.add(labelDescricao);
        painelPrincipal.add(inputDescricao);

        JPanel painelBotoes = new JPanel();
        painelBotoes.add(botaoSalvar);
        botaoSalvar.addActionListener((e) -> {
            try {
                janelaCadastroProduto.setClosed(true);
            } catch (PropertyVetoException e1) {
                e1.printStackTrace();
            }
        });
        painelBotoes.add(botaoCancelar);
        botaoCancelar.addActionListener((e) -> {
            try {
                janelaCadastroProduto.setClosed(true);
            } catch (PropertyVetoException e1) {
                e1.printStackTrace();
            }
        });
        janelaCadastroProduto.add(painelBotoes, BorderLayout.SOUTH);
    }

    private void mostrarJanela() {
        janelaCadastroProduto.pack();
        janelaCadastroProduto.setSize(800, 600);
        janelaCadastroProduto.setVisible(true);
    }

}
